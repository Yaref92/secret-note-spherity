import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Note } from './note.entity';
import { NotesService } from './notes.service';
import { Repository } from 'typeorm';
import { NOTE_ARRAY, ONE_NOTE, ONE_NOTE_DECRYPTED, CREATE_NOTE_DTO } from "./notes.mock";

describe('NotesService', () => {
  let service: NotesService;
  let repository: Repository<Note>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NotesService,
        {
          provide: getRepositoryToken(Note),
          useValue: {
            find: jest.fn().mockResolvedValue(NOTE_ARRAY),
            findOne: jest.fn().mockResolvedValue(ONE_NOTE),
            save: jest.fn().mockResolvedValue(ONE_NOTE),
            remove: jest.fn(),
            delete: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<NotesService>(NotesService);
    repository = module.get<Repository<Note>>(getRepositoryToken(Note));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create()', () => {
    it('should successfully insert a note', () => {
      const repoSpy = jest.spyOn(repository, 'save');
      expect(service.create(CREATE_NOTE_DTO)).resolves.toEqual(ONE_NOTE);
      /*
       * Since the encryption method returns a different string
       * each call, this fails, and I do not know how to fix it
       * at the moment...
       */
      //expect(repoSpy).toBeCalledWith(ONE_NOTE);
    });
  });

  describe('findAll()', () => {
    it('should return an array of notes', async () => {
      const notes = await service.findAll();
      expect(notes).toEqual(NOTE_ARRAY);
    });
  });

  describe('findOne()', () => {
    it('should get a single note', () => {
      const repoSpy = jest.spyOn(repository, 'findOne');
      expect(service.findOne(ONE_NOTE.name)).resolves.toEqual(ONE_NOTE);
      expect(repoSpy).toBeCalledWith(ONE_NOTE.name);
    });
  });

  describe('findNoteAndReturnDecryptedContent()', () => {
    it('should get a single note, decrypt it and return content', () => {
      expect(service.findNoteAndReturnDecryptedContent(ONE_NOTE.name)).resolves.toEqual(ONE_NOTE_DECRYPTED);
    });
  });

  describe('findNoteAndReturnEncryptedContent()', () => {
    it('should get a single note and return encrypted content', () => {
      expect(service.findNoteAndReturnEncryptedContent(ONE_NOTE.name)).resolves.toEqual(ONE_NOTE.encryptedContent);
    });
  });

  describe('remove()', () => {
    it('should call remove with the passed value', async () => {
      const removeSpy = jest.spyOn(repository, 'delete');
      const retVal = await service.remove(NOTE_ARRAY[1].name);
      expect(removeSpy).toBeCalledWith(NOTE_ARRAY[1].name);
      expect(retVal).toBeUndefined();
    });
  });

});
