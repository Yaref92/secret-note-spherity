import { Test, TestingModule } from '@nestjs/testing';
import { NotesController } from './notes.controller';
import { CreateNoteDto } from './dto/create-note.dto';
import { NotesService } from './notes.service';
import { NOTE_ARRAY, ONE_NOTE, CREATE_NOTE_DTO, ONE_NOTE_DECRYPTED } from "./notes.mock";

describe('NotesController', () => {
  let notesController: NotesController;
  let notesService: NotesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NotesController],
      providers: [
        NotesService,
        {
          provide: NotesService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((note: CreateNoteDto) =>
                Promise.resolve({ ...note }),
              ),
            findAll: jest.fn().mockResolvedValue(
              NOTE_ARRAY
            ),
            findOne: jest.fn().mockImplementation((name: string) =>
              Promise.resolve(
                ONE_NOTE
              ),
            ),
            findNoteAndReturnDecryptedContent: jest.fn().mockImplementation((name: string) =>
              Promise.resolve(
                ONE_NOTE_DECRYPTED
              ),
            ),
            findNoteAndReturnEncryptedContent: jest.fn().mockImplementation((name: string) =>
              Promise.resolve(
                ONE_NOTE.encryptedContent
              ),
            ),
            remove: jest.fn(),
          },
        },
      ],
    }).compile();

    notesController = module.get<NotesController>(NotesController);
    notesService = module.get<NotesService>(NotesService);
  });

  it('should be defined', () => {
    expect(notesController).toBeDefined();
  });

  describe('create()', () => {
    it('should create a note', () => {
      //notesController.create(CREATE_NOTE_DTO);
      expect(notesController.create(CREATE_NOTE_DTO)).resolves.toEqual({
        ...CREATE_NOTE_DTO,
      });
      expect(notesService.create).toHaveBeenCalledWith(CREATE_NOTE_DTO);
    });
  });

  describe('findAll()', () => {
    it('should find all notes ', () => {
      notesController.findAll();
      expect(notesService.findAll).toHaveBeenCalled();
    });
  });

  describe('findOne()', () => {
    it('should find a note, decrypt it, and return the decrypted message', () => {
      expect(notesController.findOne(ONE_NOTE.name)).resolves.toEqual(
        ONE_NOTE_DECRYPTED
      );
      expect(notesService.findNoteAndReturnDecryptedContent).toHaveBeenCalled();
    });
  });

  describe('findOneAndKeepEncrypted()', () => {
    it('should find a note, and return the encrypted content', () => {
      expect(notesController.findOneAndKeepEncrypted(ONE_NOTE.name)).resolves.toEqual(
        ONE_NOTE.encryptedContent
      );
      expect(notesService.findNoteAndReturnEncryptedContent).toHaveBeenCalled();
    });
  });

  describe('remove()', () => {
    it('should remove the note', () => {
      notesController.remove({name: NOTE_ARRAY[1].name});
      expect(notesService.remove).toHaveBeenCalled();
    });
  });

});
