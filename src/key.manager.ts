import { generateKeyPairSync } from 'crypto';
import { existsSync, writeFileSync, readFileSync } from 'fs';

const secretDirectoryPath = 'where-secrets-lie/';
const publicKeyFile = 'public.pem';
const privateKeyFile = 'private.pem';
const publicKeyPath = secretDirectoryPath+publicKeyFile;
const privateKeyPath = secretDirectoryPath+privateKeyFile;

/**
 * Checks to see if there are corresponding files for a
 * key pair where-secrets-lie.
 * If there are no files or only one, generate new pair
 * and save to files.
 * Not super safe like this, but used due to time constraints
 */
export function generateKeyPairIfNecessary() {
  if (!existsSync(publicKeyPath) || !existsSync(privateKeyPath)) {
    // If no files/ only one, generate new key pair and
    // save to files.
    const {
      publicKey,
      privateKey,
    } = generateKeyPairSync('rsa', {
      modulusLength: 4096,
      publicKeyEncoding: {
        type: 'spki',
        format: 'pem'
      },
      privateKeyEncoding: {
        type: 'pkcs8',
        format: 'pem',
        cipher: 'aes-256-cbc',
        passphrase: 'top secret'
      }
    });
    console.log(publicKey);
    writeFileSync(publicKeyPath, Buffer.from(publicKey));
    writeFileSync(privateKeyPath, Buffer.from(privateKey));
  }
}

export function retrieveEncodedPrivateKey() {
  return readFileSync(privateKeyPath);
}

export function retrievePublicKey() {
  return readFileSync(publicKeyPath);
}
