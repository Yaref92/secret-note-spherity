# build environment
FROM node:16.13.1-alpine as build
WORKDIR /app
# Add all dependencies
COPY package.json ./
RUN npm install --silent
# add actual app
COPY . ./
# build it
RUN npm run build

# production environment
# Originally wanted to use nginx, however due to time constraints switched to node
FROM nginx:stable-alpine as production
# FROM node:16.13.1-alpine as production
EXPOSE 80
COPY --from=build /app/dist /usr/share/nginx/html
CMD ["cross-env", "NODE_ENV=production", "nginx", "-g", "daemon off;"]
# CMD ["node", "dist/main"]
# CMD ["npm", "run", "start:prod"]
