import { Body, Controller, Delete, Get, Param, Post, Query } from '@nestjs/common';
import { CreateNoteDto } from './dto/create-note.dto';
import { Note } from './note.entity';
import { NotesService } from "./notes.service";

@Controller('notes')
export class NotesController {
  constructor(private readonly notesService: NotesService) {}

  @Post()
  create(@Body() createNoteDto: CreateNoteDto): Promise<Note> {
    return this.notesService.create(createNoteDto);
  }

  @Get()
  findAll(): Promise<Note[]> {
    return this.notesService.findAll();
  }

  /**
   * Gets a Note with the specified name,
   * decrypts its content and returns
   */
  @Get(':name')
  findOne(@Param('name') name: string): Promise<String> {
    return this.notesService.findNoteAndReturnDecryptedContent(name);
  }

  /**
   * Gets a Note with the specified name,
   * and returns encrypted content
   */
  @Get(':name/encrypted')
  findOneAndKeepEncrypted(@Param('name') name: string): Promise<String> {
    return this.notesService.findNoteAndReturnEncryptedContent(name);
  }

  // Prefer to have query here to avoid some weird accidental deletions
  // The type is an object literal
  @Delete()
  remove(@Query() query: {name: string}): Promise<void> {
    return this.notesService.remove(query.name);
  }

}
