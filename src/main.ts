import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { generateKeyPairIfNecessary } from './key.manager';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  generateKeyPairIfNecessary(); // Generates it at start up if necessary
  let port = process.env.NODE_ENV === 'production' ? 80 : 3000; // Listen on 80 for production and 3000 for the rest
  await app.listen(port);
}
bootstrap();
