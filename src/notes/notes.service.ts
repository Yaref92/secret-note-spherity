import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateNoteDto } from './dto/create-note.dto';
import { Note } from './note.entity';
import { encryptMessage, decryptNote } from '../encryption.service';

@Injectable()
export class NotesService {
  constructor(
    @InjectRepository(Note)
    private readonly notesRepository: Repository<Note>,
  ) {}

  create(createNoteDto: CreateNoteDto): Promise<Note> {
    const note = new Note();
    note.name = createNoteDto.name;
    note.encryptedContent = encryptMessage(createNoteDto.plainTextMessage);
    return this.notesRepository.save(note);
  }

  async findAll(): Promise<Note[]> {
    return this.notesRepository.find();
  }

  /**
   * This method uses the Repository to
   * find the note with this name.
   * this returnes Promise<Note | undefined>.
   */
  findOne(name: string): Promise<Note | undefined> {
    return this.notesRepository.findOne(name);
  }

  /**
   * This method wraps the call to the repository and
   * decrypts the content before returning Promise<String>
   */
  findNoteAndReturnDecryptedContent(name: string): Promise<String> {
    return new Promise<String>((resolve, reject) =>  {
      this.findOne(name).then(note => {
        if (typeof note === "undefined") {
          reject("No note found with the name " + name);
        } else {
          resolve(decryptNote(note));
        }
      });
    });
  }

  /**
   * This method wraps the call to the repository and
   * returs a Promise<String> of the encrypted content
   */
  findNoteAndReturnEncryptedContent(name: string): Promise<String> {
    return new Promise<String>((resolve, reject) =>  {
      this.findOne(name).then(note => {
        if (typeof note === "undefined") {
          reject("No note found with the name " + name);
        } else {
          resolve(note.encryptedContent);
        }
      });
    });
  }

  async remove(name: string): Promise<void> {
    await this.notesRepository.delete(name);
  }
}
