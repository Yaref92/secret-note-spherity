import { Note } from './notes/note.entity';
import { retrieveEncodedPrivateKey, retrievePublicKey } from './key.manager';
import { privateDecrypt, publicEncrypt, constants as cryptoConstants } from 'crypto';

// The message is encrypted here before being sent to the database
export function encryptMessage(plainTextMessage: string): string {
  let publicKeyString: Buffer = retrievePublicKey();
  return publicEncrypt(
    {
      key: publicKeyString,
      padding: cryptoConstants.RSA_PKCS1_OAEP_PADDING,
      oaepHash: "sha256",
    }, Buffer.from(plainTextMessage)
  ).toString("base64");
}

// The note's content is decrypted and returned
export function decryptNote(note: Note): string {
  let encodedPrivateKeyString: Buffer = retrieveEncodedPrivateKey();
  return privateDecrypt({
    key:  encodedPrivateKeyString,
    passphrase: 'top secret',
    padding: cryptoConstants.RSA_PKCS1_OAEP_PADDING,
    oaepHash: "sha256",
  }, Buffer.from(note.encryptedContent, "base64")).toString();
}
