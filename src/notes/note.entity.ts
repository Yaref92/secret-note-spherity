import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity("notes")
export class Note {
  @PrimaryColumn()
  name: string;

  @Column()
  encryptedContent: string;
}
