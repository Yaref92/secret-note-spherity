import { CreateNoteDto } from "./dto/create-note.dto";
import { encryptMessage} from '../encryption.service';

const firstMessage: string = 'I am a note';
const firstName: string = 'Name #1';
// Using encryptMessage here introduces a problem:
// This encryption method returns a different string every time.
// As of yet, I do not know what to do about it, but will leave it here.
const encryptedFirstNote: string = encryptMessage(firstMessage);

export const NOTE_ARRAY = [
  {
    name: firstName,
    encryptedContent: encryptedFirstNote,
  },
  {
    name: 'Name #2',
    encryptedContent: encryptMessage('I am a note too'),
  },
];

export const ONE_NOTE = {
  name: firstName,
  encryptedContent: encryptedFirstNote,
};

export const CREATE_NOTE_DTO : CreateNoteDto = {
  name: firstName,
  plainTextMessage: firstMessage,
}

export const ONE_NOTE_DECRYPTED = firstMessage;
