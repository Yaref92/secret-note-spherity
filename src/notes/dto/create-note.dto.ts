export class CreateNoteDto {
  name: string;
  plainTextMessage: string; // The message is entered by the user as plain text
}
